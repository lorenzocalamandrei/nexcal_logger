# Nexcal Logger
---

## What does he do?
Create an "advanced" log file, where you can enrich information regarding an error message applied by code (addError, addWarning).

Example:
```log
2018-10-18T13:10:50+00:00 DEBUG (7): {nexcal_5bc8865a698d9} Start addError
2018-10-18T13:10:50+00:00 DEBUG (7): {nexcal_5bc8865a698d9} 	From session: customer/session
2018-10-18T13:10:50+00:00 DEBUG (7): {nexcal_5bc8865a698d9} 	Method called by: 
2018-10-18T13:10:50+00:00 DEBUG (7): {nexcal_5bc8865a698d9} 		File: /var/www/htdocs/test.php
2018-10-18T13:10:50+00:00 DEBUG (7): {nexcal_5bc8865a698d9} 		Line: 5
2018-10-18T13:10:50+00:00 DEBUG (7): {nexcal_5bc8865a698d9} 		Apparent reason: Example
2018-10-18T13:10:50+00:00 DEBUG (7): {nexcal_5bc8865a698d9} 	Customer/Admin non identified (guest or exception logged).
2018-10-18T13:10:50+00:00 DEBUG (7): {nexcal_5bc8865a698d9} End addError

2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} Start addError
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} 	From session: customer/session
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} 	Method called by: 
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} 		File: /var/www/htdocs/app/code/core/Mage/Customer/controllers/AccountController.php
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} 		Line: 1004
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} 		Apparent reason: Invalid current password
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} 	Called from frontend panel. Customer Logged as: 
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} 		[ID] - 141
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} 		[EMAIL] - email@example.com
2018-10-18T13:13:37+00:00 DEBUG (7): {nexcal_5bc8870131bc5} End addError

2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} Start addError
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} 	From session: adminhtml/session
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} 	Method called by: 
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} 		File: /var/www/htdocs/app/code/core/Mage/Adminhtml/controllers/System/AccountController.php
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} 		Line: 80
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} 		Apparent reason: Invalid current password.
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} 	Called from backend panel. Admin Logged as: 
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} 		[ID] - 8
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} 		[EMAIL] - admin
2018-10-18T13:16:30+00:00 DEBUG (7): {nexcal_5bc887ae0a955} End addError
```

## How to use
This is just one example of middleware with the main Session classes to create the most accurate logs from an addError or addWarning.

In case of malfunction check the rewrite present in the config.xml

This module only serves for debugging suggestions in prod or similar.

```xml
<adminhtml>
    <rewrite>
        <session>Nexcal_Logger_Model_Rewrite_Adminhtml_Session</session>
    </rewrite>
</adminhtml>
<core>
    <rewrite>
        <session>Nexcal_Logger_Model_Rewrite_Core_Session</session>
    </rewrite>
</core>
<customer>
    <rewrite>
        <session>Nexcal_Logger_Model_Rewrite_Customer_Session</session>
    </rewrite>
</customer>
<checkout>
    <rewrite>
        <session>Nexcal_Logger_Model_Rewrite_Checkout_Session</session>
    </rewrite>
</checkout>
```

```php
/**
 * logFromCore
 * @param string $errorType
 * @return bool
 */
public function logFromCore($errorType);
```

## Settings
System > Configuration > Nexcal Modules > Nexcal Logger >
 - enabled: yes/no

---

* developed by Lorenzo Calamandrei <nexcal.dev@gmail.com>