<?php
/**
 * Nexcal_Logger
 */

/**
 * Class Nexcal_Logger_Helper_Data
 * @author Lorenzo Calamandrei <nexcal.dev@gmail.com>
 * @version 0.1.0
 * @package NexCal_Logger
 */
class Nexcal_Logger_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * @const LOG_FILE where log of NexCal_Logger
	 */
	const LOG_FILE = 'nexcal_logger.log';

	/**
	 * @const LOG LEVEL.
	 */
	const LOG_ERROR = 1,
		LOG_CRITICAL = 2,
		LOG_DEBUG = 5,
		LOG_INFO = 7;

	/**
	 * @const LOG_SEPARATOR
	 */
	const LOG_SEPARATOR = '------------------';

	/**
	 * @var string $NEXCAL_ERR
	 */
	public $NEXCAL_ERR = 'Error';

	/**
	 * @var string $NEXCAL_WARN
	 */
	public $NEXCAL_WARN = 'Warning';

	/**
	 * @var $from
	 */
	public $from;

	/**
	 * getConfig
	 * @param string $code
	 * @return mixed
	 */
	public function getConfig($code)
	{
		return Mage::getStoreConfig('nexcal_logger/' . $code);
	}

	/**
	 * isEnabled
	 * @return mixed
	 */
	public function isEnabled()
	{
		return $this->getConfig('settings/enabled');
	}

	/**
	 * log
	 * @param $obj
	 * @param int $importance
	 * @return bool
	 */
	public function log($id, $obj, $importance, $level = 0)
	{
		if ($importance > 1
			&& !$this->isEnabled()
			&& !$this->getConfig('settings/log_enabled')
		) {
			return false;
		}

		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			$indent .= "\t";
		}

		if (is_string($obj)) {
			Mage::log("{{$id}} " . $indent . $obj, $importance, self::LOG_FILE, true);
		} else {
			Mage::log("PROCESSOR ERROR/WARNING ID FOR OBJ: {$id}", $importance, self::LOG_FILE, true);
			Mage::log($obj, $importance, self::LOG_FILE);
		}

		return true;
	}

	/**
	 * logFromCore
	 * @param string $errorType
	 * @return bool
	 */
	public function logFromCore($errorType)
	{
		$uniquer = uniqid('nexcal_', false);
		$this->log($uniquer, "Start add$errorType", self::LOG_INFO);
		$this->log($uniquer, "From session: {$this->from}/session", self::LOG_INFO, 1);

		// 1 for skip this method.
		$debugBackTrace = @debug_backtrace()[1];

		// retrieved the debug backtrace?
		if ($debugBackTrace === null) {
			$this->log($uniquer, 'Cannot retrieve the backtrace.', self::LOG_CRITICAL, 1);
			$this->log(self::LOG_SEPARATOR, self::LOG_CRITICAL, 1);
			return false;
		}

		$this->log($uniquer, 'Method called by: ', self::LOG_INFO, 1);
		$this->log($uniquer, "File: {$debugBackTrace['file']}", self::LOG_INFO, 2);
		$this->log($uniquer, "Line: {$debugBackTrace['line']}", self::LOG_INFO, 2);
		if (@is_string(@$debugBackTrace['args'][0])) {
			$this->log($uniquer, "Apparent reason: {$debugBackTrace['args'][0]}", self::LOG_INFO, 2);
		}

		// retrieve the admin store true/false
		try {
			$adminStore = Mage::app()->getStore()->isAdmin();
		} catch (Exception $e) {
			$this->log($uniquer, 'Cannot retrieve the store.', self::LOG_CRITICAL, 1);
			$adminStore = false;
		}

		if ($adminStore) {
			// is admin? Log admin data
			$adminUser = Mage::getSingleton('admin/session')->getUser();
			$this->log($uniquer, 'Called from backend panel. Admin Logged as: ', self::LOG_INFO, 1);
			$this->log($uniquer, '[ID] - ' . $adminUser->getId(), self::LOG_INFO, 2);
			$this->log($uniquer, '[USERNAME] - ' . $adminUser->getUsername(), self::LOG_INFO, 2);
		} else {
			// is customer? Log customer data
			$customer = null;
			$customerSession = Mage::getSingleton('customer/session');

			if ($customerSession->isLoggedIn()) {
				$customer = $customerSession->getCustomer();
				$this->log($uniquer, 'Called from frontend panel. Customer Logged as: ', self::LOG_INFO, 1);
				$this->log($uniquer, '[ID] - ' . $customer->getId(), self::LOG_INFO, 2);
				$this->log($uniquer, '[EMAIL] - ' . $customer->getEmail(), self::LOG_INFO, 2);
			} else {
				$this->log($uniquer, 'Customer/Admin non identified (guest or exception logged).', self::LOG_INFO, 1);
			}
		}

		$this->log($uniquer, "End add$errorType\n", self::LOG_INFO);
		return true;
	}
}
