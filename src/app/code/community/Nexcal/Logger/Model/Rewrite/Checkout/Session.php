<?php
/**
 * Nexcal_Logger
 */

/**
 * Class Nexcal_Logger_Model_Rewrite_Checkout_Session
 * @author Lorenzo Calamandrei <nexcal.dev@gmail.com>
 * @version 0.1.0
 * @package NexCal_Logger
 */
class Nexcal_Logger_Model_Rewrite_Checkout_Session extends Mage_Checkout_Model_Session
{
	/**
	 * @var Mage_Core_Helper_Abstract|Nexcal_Logger_Helper_Data
	 */
	protected $nexcalHelper;

	/**
	 * Nexcal_Logger_Model_Rewrite_Core_Session constructor.
	 * @param array $data
	 */
	public function __construct(array $data = array())
	{
		parent::__construct($data);
		$this->nexcalHelper = Mage::helper('nexcal_logger');
		$this->nexcalHelper->from = 'checkout';
	}

	/**
	 * Adding new error message
	 * @param   string $message
	 * @return  Mage_Core_Model_Session_Abstract
	 */
	public function addError($message)
	{
		$this->nexcalHelper->logFromCore($this->nexcalHelper->NEXCAL_ERR);
		$this->addMessage(Mage::getSingleton('core/message')->error($message));
		return $this;
	}

	/**
	 * Adding new warning message
	 * @param   string $message
	 * @return  Mage_Core_Model_Session_Abstract
	 */
	public function addWarning($message)
	{
		$this->nexcalHelper->logFromCore($this->nexcalHelper->NEXCAL_WARN);
		$this->addMessage(Mage::getSingleton('core/message')->warning($message));
		return $this;
	}
}