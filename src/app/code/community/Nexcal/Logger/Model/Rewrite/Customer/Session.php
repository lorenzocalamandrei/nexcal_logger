<?php
/**
 * Nexcal_Logger
 */

/**
 * Class Nexcal_Logger_Model_Rewrite_Customer_Session
 * @author Lorenzo Calamandrei <nexcal.dev@gmail.com>
 * @version 0.1.0
 * @package NexCal_Logger
 */
class Nexcal_Logger_Model_Rewrite_Customer_Session extends Mage_Customer_Model_Session
{
	/**
	 * @var Mage_Core_Helper_Abstract|Nexcal_Logger_Helper_Data
	 */
	protected $nexcalHelper;

	/**
	 * Nexcal_Logger_Model_Rewrite_Core_Session constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->nexcalHelper = Mage::helper('nexcal_logger');
		$this->nexcalHelper->from = 'customer';
	}

	/**
	 * Adding new error message
	 * @param   string $message
	 * @return  Mage_Core_Model_Session_Abstract
	 */
	public function addError($message)
	{
		$this->nexcalHelper->logFromCore($this->nexcalHelper->NEXCAL_ERR);
		$this->addMessage(Mage::getSingleton('core/message')->error($message));
		return $this;
	}

	/**
	 * Adding new warning message
	 * @param   string $message
	 * @return  Mage_Core_Model_Session_Abstract
	 */
	public function addWarning($message)
	{
		$this->nexcalHelper->logFromCore($this->nexcalHelper->NEXCAL_WARN);
		$this->addMessage(Mage::getSingleton('core/message')->warning($message));
		return $this;
	}
}
